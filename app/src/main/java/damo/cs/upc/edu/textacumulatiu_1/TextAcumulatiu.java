package damo.cs.upc.edu.textacumulatiu_1;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * TextAcumulatiu
 */


public class TextAcumulatiu extends Activity {
    EditText camp;
    TextView res;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_acumulatiu);

        //  setContentView(R.layout.activity_constraint_layout);

          setContentView(R.layout.activity_constraint_layout_dos);

        inicialitza();


    }

    private void inicialitza() {
        camp = findViewById(R.id.camp);
        res =  findViewById(R.id.resultat);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.text_acumulatiu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void nouContingut(View v) {
                afegeixAResultat(camp.getText());
                camp.setText("");
    }

    private void afegeixAResultat(CharSequence text) {
        res.append("\n");
        res.append(text);
    }

    public void esborraResultat(View v) {
        res.setText("");
        camp.setText("");
    }

}
